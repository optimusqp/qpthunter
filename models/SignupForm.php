<?php
/**
 * Created by PhpStorm.
 * User: optimusqp
 * Date: 25.04.2018
 * Time: 14:39
 */
namespace app\models;
use yii\base\Model;

class SignupForm extends Model{

    public $username;
    public function rules()
    {
        return[

            [['username'], 'required', 'message' => 'Заполните поля'],
            ['username', 'unique', 'targetClass' => User::class, 'message' => 'Этот ник уже занят!','filter' => ['!=','username' ,'username']],
        ];
    }
    public function attributeLabels()
    {
        return[
            'username' => 'Имя',
        ];
    }

}