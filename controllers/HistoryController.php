<?php

namespace app\controllers;

use app\models\SignupForm;
use app\models\User;
use app\models\History;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class HistoryController extends Controller
{
    public function actionIndex()
    {

        $username_from = Yii::$app->user->identity->username;
        $history_log = History::find()
            ->where(['usernamefrom' => $username_from])
            ->orderBy(['ID'=>SORT_DESC])
            ->asArray()->all();
        return $this->render('index',compact('history_log','username_from'));

    }
}