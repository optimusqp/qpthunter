<?php

namespace app\controllers;

use app\models\History;
use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $users = User::find()
            ->orderBy(['ID'=>SORT_DESC])->limit(2000)->all();



        $username_from = Yii::$app->user->identity->username;
        $POST_array=Yii::$app->request->post();


        if ($POST_array['send_money']) {

            $to_username = $POST_array['to_username'];

            if ($to_username != $username_from){
                $query_for_temp_balance = User::find()
                    ->where(['username' => $username_from])
                    ->all();


                $how_users_with_name = User::find()
                    ->where(['username'=>$to_username])
                    ->count();

            $temp_balance = $query_for_temp_balance[0]->balance;

            $how_money = floatval($POST_array['how_money']);

            if ($how_money < 0) $how_money = 0;

            $new_balance = $temp_balance - $how_money;


            if (($temp_balance > -1000) and ($new_balance > -1000)){


                if ($how_users_with_name==0){

                    $user = new User();
                    $user->username = $to_username;

                    if ($to_username=='') return $this->goHome();
                    if ($to_username==NULL) return $this->goHome();

                    $user->save();

                }

                Yii::$app->db->createCommand("UPDATE `optimusqp_optimusqprudb`.`user` SET balance=" . $new_balance . " WHERE username ='" . $username_from . "'")->execute();
                Yii::$app->db->createCommand("UPDATE `optimusqp_optimusqprudb`.`user` SET balance=balance+" . $how_money . " WHERE username ='" . $to_username . "'")->execute();
                Yii::$app->db->createCommand("INSERT INTO `optimusqp_optimusqprudb`.`history` (ID, usernamefrom, usernameto,balance) VALUES (NULL,'".$username_from."', '" . $to_username . "','" . $new_balance. "');")->execute();

                return $this->goHome();

            }

        }

        }


        return $this->render('index', compact('users', 'username_from'));

    }


    public function actionHistory()
    {

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }



    public function actionSignup(){
        if(!Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }
        $model = new SignupForm();
        if($model->load(\Yii::$app->request->post()) && $model->validate())
        {
            $user = new User();
            $user->username = $model->username;

            if($user->save())
            {
                return $this->goHome();
            }
        }
        return $this->render('signup', compact('model'));
    }
}
