<?php
use yii\widgets\Pjax;
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin(['layout' => 'horizontal']);
$this->title = 'Hello '.$username_from."!";
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Super Money Transfer!</h1>

        <p class="lead">We know a lot about money transfers!</p>

    </div>

    <div class="body-content">

        <div class="row">

            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">


                    <h2>Transfer for new member:</h2>
                    <?php Pjax::begin(['enablePushState'=>false,]); ?>
                    <?= Html::beginForm(['/site/index'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
                    Member: <?= Html::input('text', 'to_username', $start_value, ['class' => 'form-control']) ?>
                    Sum: <?= Html::input('text', 'how_money', $start_value, ['class' => 'form-control']) ?>
                        <?= Html::hiddenInput('send_money', 'true');?>
                        <?= Html::submitButton('Send $$$', ['class' => 'btn btn-lg btn-primary']) ?>

                    <?= Html::endForm() ?>
                    <?php Pjax::end(); ?>

                    <br>

                    <div class="row"><div class="col-sm-12">




                            <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 203.75px;" aria-label="Browser: activate to sort column ascending">
                                        User
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.567px;" aria-label="Platform(s): activate to sort column ascending">
                                        Balance
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.567px;" aria-label="Platform(s): activate to sort column ascending">
                                        Sum
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.567px;" aria-label="Platform(s): activate to sort column ascending">
                                        SendGo!
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php

                                $toggle==0;

                                foreach ($users as $user){

                                    if ($toggle==1){
                                        echo '<tr role="row" class="odd">';
                                        $toggle=2;
                                    }else{
                                        echo '<tr role="row" class="even">';
                                        $toggle=1;
                                    }

                                    $username = $user->username;

                                    echo "<td class=\"sorting_1\">".$username."</td>";

                                    echo "<td>".$user->balance."</td>"; ?>
                                    <?php Pjax::begin(['enablePushState'=>false,]); ?>
                                        <td><?= Html::beginForm(['/site/index'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
                                        <?= Html::input('text', 'how_money', $start_value, ['class' => 'form-control']) ?>
                                        <?= Html::hiddenInput('to_username', $username);?>
                                        <?= Html::hiddenInput('send_money', 'true');?>

                                        </td>
                                        <td>
                                        <?= Html::submitButton('Send $$$', ['class' => 'btn btn-lg btn-primary']) ?>
                                        </td>
                                        <?= Html::endForm() ?>
                                        <?php Pjax::end(); ?>
                                    <?php

                                    echo "</tr>";
                                }
                                ?>


                                </tbody>
                                <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">
                                        User
                                    </th>
                                    <th rowspan="1" colspan="1">
                                        Balance
                                    </th>

                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.567px;" aria-label="Platform(s): activate to sort column ascending">
                                        Sum
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.567px;" aria-label="Platform(s): activate to sort column ascending">
                                        SendGo!
                                    </th>
                                </tr>
                                </tfoot>
                            </table>


                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
