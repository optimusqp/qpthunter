<?php
use yii\widgets\Pjax;
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin(['layout' => 'horizontal']);
$this->title = 'Hello '.$username_from."!";
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>History operation</h1>


    </div>

    <div class="body-content">

        <div class="row">

            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">


                    <div class="row"><div class="col-sm-12">

                            <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 203.75px;" aria-label="Browser: activate to sort column ascending">
                                        ID
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 203.75px;" aria-label="Browser: activate to sort column ascending">
                                        User-recipient
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.567px;" aria-label="Platform(s): activate to sort column ascending">
                                        Your balance
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.567px;" aria-label="Platform(s): activate to sort column ascending">
                                        Sum
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php

                                $toggle==0;

                                foreach ($history_log as $history_) {


                                        if ($toggle == 1) {
                                            echo '<tr role="row" class="odd">';
                                            $toggle = 2;
                                        } else {
                                            echo '<tr role="row" class="even">';
                                            $toggle = 1;
                                        }

                                        echo "<td class=\"sorting_1\">".$history_['ID']."</td>";
                                        echo "<td>".$history_['usernamefrom']."</td>";
                                        echo "<td>".$history_['usernameto']."</td>";
                                        echo "<td>".$history_['balance']."</td>";
                                        echo "</tr>";/**/

                                }
                                ?>


                                </tbody>
                                <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">
                                        ID
                                    </th>
                                    <th rowspan="1" colspan="1">
                                        User-recipient
                                    </th>
                                    <th rowspan="1" colspan="1">
                                        Your balance
                                    </th>

                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 179.567px;" aria-label="Platform(s): activate to sort column ascending">
                                        Sum
                                    </th>
                                </tr>
                                </tfoot>
                            </table>

                            <?php  ?>
                        </div></div>

                </div>
            </div>




        </div>

    </div>
</div>
